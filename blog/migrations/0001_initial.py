# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('post_title', models.CharField(max_length=120)),
                ('short_description', models.CharField(max_length=200)),
                ('post', models.TextField(max_length=20000)),
                ('date', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name_plural': 'Статті',
                'verbose_name': 'статтю',
            },
        ),
    ]
