from django.db import models

class Post(models.Model):
    post_title = models.CharField(max_length=120)
    short_description = models.CharField(max_length=200)
    post = models.TextField(max_length=20000)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.post_title

    class Meta:
        verbose_name = 'статтю'
        verbose_name_plural = 'Статті'
