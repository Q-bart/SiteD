from django.shortcuts import render, redirect
from blog.models import Post

def index(request):
    context = {
        'posts': Post.objects.filter()
    }
    return render(request, 'index.html', context)

def post(request, post_id):
    context = {
        'posts': Post.objects.get(id=post_id)
    }

    return render(request, 'post.html', context)
