from django.shortcuts import render, redirect
from main.forms import UserRegisterForm
from django.contrib import auth

def register(request):
    context = {
        'form': UserRegisterForm()
    }
    if request.POST:
        newuser_form = UserRegisterForm(request.POST)
        if newuser_form.is_valid():
            newuser_form.save()
            newuser = auth.authenticate(username=newuser_form.cleaned_data['username'],
                password = newuser_form.cleaned_data['password1'])
            auth.login(request, newuser)
            return redirect('/')
        else:
            context['form'] = newuser_form
    return render(request, 'register.html', context)

def login(request):
    if request.POST:
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return redirect('/')
        else:
            args = {
                'login_error' : 'Користувача не знайдено'
                }
            return render(request, 'login.html', args)

    return render(request, 'login.html')

def base(request):
    return render(request, 'main.html')

def logout(request):
    auth.logout(request)
    return redirect('/')
