from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class UserRegisterForm(UserCreationForm):
    username = forms.CharField(label=u"Ім'я користувача", max_length=50)
    email = forms.EmailField(label=u"E-mail")
    password1 = forms.CharField(label=u'Пароль', widget=forms.PasswordInput)
    password2 = forms.CharField(label=u'Підтвердження паролю', widget=forms.PasswordInput)
